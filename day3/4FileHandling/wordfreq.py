def freqq(input_filename, output_filename):
    word_frequency = {}
    try:
        input_file = open(input_filename, "r")
        content = input_file.read()
        input_file.close()

        words = content.split()

        for word in words:
            word = word.lower()
            word_frequency[word] = word_frequency.get(word, 0) + 1

        output_file = open(output_filename, "w")
        for word, frequency in word_frequency.items():
            output_file.write(f"{word}: {frequency}\n")
        output_file.close()

        print("Word frequencies written to", output_filename)

    except FileNotFoundError:
        print("Input file not found.")
    except Exception as e:
        print("An error occurred:", e)

input_file = "text.txt"
output_file = "output.txt"
freqq(input_file, output_file)
