class Employee:
    def __init__(self, name, salary):
        self.name = name
        self._salary = salary
        self.__bonus = 0

    def display_info(self):
        print(f"Name: {self.name}, Salary: {self._salary}, Bonus: {self.__bonus}")

    def __calculate_bonus(self):
        self.__bonus = self._salary * 0.1

    def calculate_and_display_bonus(self):
        self.__calculate_bonus()
        self.display_info()

e = Employee("Rajesh", 50000)

print(e.name)
print(e._salary)

e.display_info()
e.calculate_and_display_bonus()

'''
name is a public attribute
_salary is a protected attribute
__bonus is a private attribute(mangled)
__calculate_bonus is a private method
'''