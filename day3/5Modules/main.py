import exmath

# Using functions from the imported module
result_add = exmath.add(5, 3)
result_multiply = exmath.multiply(4, 6)

print("Addition Result:", result_add)
print("Multiplication Result:", result_multiply)
