from shapes import circle, rectangle

l = 4
w = 6
rarea = rectangle.area(l, w)
print("Rectangle Area:", rarea)

r = 5
carea = circle.area(r)
print("Circle Area:", carea)

