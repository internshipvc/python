class NegError(Exception):
    def __init__(self, number):
        self.number = number
        super().__init__(f"Negative number not allowed: {self.number}")

def sqroot(num):
    if num < 0:
        raise NegError(num)
    return num ** 0.5

try:
    number = float(input("Enter a number: "))
    result = sqroot(number)
    print(f"Square root of {number} is {result}")
except NegError as e:
    print(e)
except ValueError:
    print("Invalid input. Please enter a valid number.")
