from circle import Circle
from square import Square

circle = Circle(5)
square = Square(4)

print("Circle Area:", circle.area())
print("Square Area:", square.area())
