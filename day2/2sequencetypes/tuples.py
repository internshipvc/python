# Creating Tuples
tuple1 = (10, 20, 30)

# Accessing and Slicing
print(tuple1[1])
print(tuple1[1:3])

#Repetition
tuple_repeat = tuple1 * 2
print(tuple_repeat)

# Nested Tuples
nested_tuple = ((1, 2), (3, 4), (5, 6))
print(nested_tuple[1][0])

# Length and Membership
print(len(tuple1))
print(20 in tuple1)

# Unpacking Tuples
a, b, c = tuple1

# Finding Min and Max
num_tuple = (5, 3, 8, 1, 9)
print("Min:", min(num_tuple))
print("Max:", max(num_tuple))

# Count and Index
count_twos = num_tuple.count(2)
index_five = num_tuple.index(5)
print("Count of 2:", count_twos)
print("Index of 5:", index_five)
