name1 = "Aniket, "
name2 = "Snehal!"
print(name1 + name2)
print(name1 * 3)

print(name1[0])
print(name2[2:5])

print(len(name1))

print(name1.upper())
print(name2.lower())
print(name1.strip())
print(name1.replace("A", "S"))

person_name = "Amruta"
person_age = 28
formatted_s = f"My name is {person_name} and I'm {person_age} years old."
print(formatted_s)

fruit_list = "India is my country".split()
joined_fruits = "-".join(fruit_list)
print(joined_fruits)
