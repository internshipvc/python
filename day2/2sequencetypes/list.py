my_list = [1, 2, 'Bhooshan', 10.5, -30]

print(my_list)
print(my_list[3])
print(my_list[3:5])
print(my_list * 2)
print(len(my_list))

my_list.append(40)
my_list.remove('Bhooshan')
del my_list[1]
print(my_list)

#Functions
print(max(my_list))
print(min(my_list))

my_list.insert(3, 99)
my_list.insert(10, 1001)
print(my_list)

my_list.sort()
print(my_list)
my_list.sort(reverse=True)
print(my_list)

another_list = ['A', 'C', 'B']
another_list.sort()
print(another_list)
