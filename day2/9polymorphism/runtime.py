class Shape:
    def calculate_area(self):
        pass

class Circle(Shape):
    def __init__(self, radius):
        self.radius = radius

    def calculate_area(self):
        return 3.14 * self.radius * self.radius

class Rectangle(Shape):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def calculate_area(self):
        return self.width * self.height

def print_area(shape):
    print("Area:", shape.calculate_area())

circle = Circle(5)
rectangle = Rectangle(4, 6)

#Runtime
print_area(circle)
print_area(rectangle)
