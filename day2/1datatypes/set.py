set1 = {1, 2, 3}
set2 = {2, 3, 4}

union_set = set1 | set2
intersection_set = set1 & set2
difference_set = set1 - set2
symmetric_diff_set = set1 ^ set2

print("Union:", union_set)
print("Intersection:", intersection_set)
print("Difference:", difference_set)
print("Symmetric Difference:", symmetric_diff_set)

set1.add(5)
set1.remove(2)
set2.discard(4)

print("Modified Set 1:", set1)
print("Modified Set 2:", set2)

print(set1.issubset(set2))
print(set2.issuperset(set1))
print(set1.isdisjoint(set2))

set_copy = set1.copy()
print("Copied Set:", set_copy)
