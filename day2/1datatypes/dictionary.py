ps = {'Virat': 120, 'Rohit': 85, 'Bumrah': 0}

print(ps['Virat'])
ps['Rohit'] = 100

ps['Shikhar Dhawan'] = 70

del ps['Bumrah']

print('Rohit' in ps)

# Dictionary Methods
print(len(ps))
print(ps.keys())
print(ps.values())
print(ps.items())

for player, score in ps.items():
    print(f"{player}: {score}")

teams = {
    'India': {'Virat': 120, 'Rohit': 85},
    'Australia': {'Smith': 98, 'Warner': 75}
}

print(teams['India']['Rohit'])


#Merging
dict1 = {'a': 1, 'b': 2}
dict2 = {'b': 3, 'c': 4}
merged_dict = {**dict1, **dict2}
print(merged_dict)
