int_val = 42
str_val = str(int_val)
print(str_val)
print(type(str_val))

print()
float_val = 3.14
int_from_float = int(float_val)
int_to_float = float(int_val)
print(int_from_float)
print(int_to_float)

print()
str_float = "2.71828"
float_val = float(str_float)
print(float_val)

print()
bool_from_int = bool(int_val)
print(bool_from_int)

#bin()
#hex()
#oct()