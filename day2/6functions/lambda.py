square_lambda = lambda x: x ** 2
print(square_lambda(5))

# Using lambda with map
numbers = [1, 2, 3, 4, 5]
squared_numbers = list(map(lambda x: x ** 2, numbers))
print(squared_numbers)

# Using lambda with filter
even_numbers = list(filter(lambda x: x % 2 == 0, numbers))
print(even_numbers)

# Using lambda with sorted
names = ["Bhooshan", "Sanket", "Prateek", "Rahul"]
sorted_names = sorted(names, key=lambda word: len(word))
print(sorted_names)
