#Normal Function
def greet(name):
    print(f"Hello, {name}!")

greet("Bhooshan")

# Function with return statement
def add(x, y):
    return x + y

result = add(5, 3)
print(result)

# Default arguments
def greet_default(name="Guest"):
    print(f"Hello, {name}!")

greet_default()
greet_default("Sanket")
greet_default(name="Prateek")

# Arbitrary number of arguments
def print_items(*args):
    for item in args:
        print(item, end=" ")
    print()

print_items("Apple", "Banana", "Cherry")

# Arbitrary number of keyword arguments
def print_dict(**kwargs):
    for key, value in kwargs.items():
        print(f"{key}: {value}")

print_dict(name="Rahul", age=25)

# Passing a function as an argument
def apply_operation(operation, x, y):
    return operation(x, y)

def multiply(a, b):
    return a * b

result = apply_operation(multiply, 3, 4)

