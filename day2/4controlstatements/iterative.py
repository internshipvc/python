#for loop
for i in range(5):
    print(" =", i)

#while loop
j = 0
while j < 5:
    print("j =", j)
    j += 1
