# Using break
for i in range(5):
    if i == 3:
        print("Breaks loop at i =", i)
        break
    print("i =", i)

print()
# Using continue
for i in range(5):
    if i == 2:
        print("Skips iteration at i =", i)
        continue
    print("i =", i)

print()
# Using pass
for i in range(5):
    if i == 2:
        pass #placeholder for future code
    else:
      print("i =", i)

