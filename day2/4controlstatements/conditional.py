#if, elif, else
age = int(input("Enter your age: "))

if age < 18:
    ticket_price = 10
elif age >= 18 and age < 60:
    ticket_price = 20
else:
    ticket_price = 15

print(f"Your ticket price is ${ticket_price}")


#nested if else
total = float(input("Enter total purchase amount: "))
is_member = input("Are you a member? (yes/no): ")

if is_member == "yes":
    if total >= 100:
        discount = 0.1
    else:
        discount = 0.05
else:
    discount = 0.02

final_amount = total - (total * discount)
print(f"Final amount after discount: ${final_amount:.2f}")
