import sys

script_name = sys.argv[0]
argument1 = sys.argv[1] if len(sys.argv) > 1 else None
argument2 = sys.argv[2] if len(sys.argv) > 2 else None

print("Script Name:", script_name)
print("Argument 1:", argument1)
print("Argument 2:", argument2)
print("Total length of arguments :", len(sys.argv))
