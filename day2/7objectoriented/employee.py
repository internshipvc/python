class Employee:
    dept = 'CDN' #static variable

    def __init__(self, id, name):
        self.name = name
        self.id = id

e1 = Employee(1,'Abhishek Kumar')

print(e1.name)
print(e1.id)
print(e1.dept) #static variable using object name
print(Employee.dept) #staic variable using class name