# Arithmetic Operators
print("Addition:", 10 + 3)
print("Subtraction:", 10 - 3)
print("Multiplication:", 10 * 3)
print("Division:", 10 / 3)
print("Floor Division:", 10 // 3)
print("Modulus:", 10 % 3)
print("Exponentiation:", 10 ** 3)

# Comparison Operators
print("Greater Than:", 10 > 3)
print("Less Than:", 10 < 3)
print("Equal To:", 10 == 3)
print("Not Equal To:", 10 != 3)
print("Greater Than or Equal To:", 10 >= 3)
print("Less Than or Equal To:", 10 <= 3)

# Logical Operators
print("Logical AND:", True and False)
print("Logical OR:", True or False)
print("Logical NOT:", not True)

# Bitwise Operators
print("Bitwise AND:", 10 & 3)
print("Bitwise OR:", 10 | 3)
print("Bitwise XOR:", 10 ^ 3)
print("Bitwise NOT:", ~10)
print("Left Shift:", 10 << 2)
print("Right Shift:", 10 >> 2)

# Assignment Operators
x = 5
x += 3
y = 10
y -= 2
z = 6
z *= 4
w = 15
w /= 3

# Identity Operators
print("Identity IS:", 10 is 3)
print("Identity IS NOT:", 10 is not 3)

# Membership Operators
print("Membership IN:", 'a' in 'apple')
print("Membership NOT IN:", 'b' not in 'apple')

# Ternary Operator
print("Ternary Result:", 5 if 10 > 3 else 10)

# String Concatenation
print("Concatenated String:", "Hello, " + "World!")

# List Concatenation
print("Concatenated List:", [1, 2, 3] + [4, 5, 6])

# Conditional Expression
print("Max Value:", 10 if 10 > 3 else 3)

# Chained Comparison
print("Between:", 1 < 10 < 3)
print("Between:", 1 < 10 < 13)
